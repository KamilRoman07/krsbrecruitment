using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "InteractableBaseData", menuName = "Data/InteractableBaseData", order = 1)]
    public class InteractableBaseData : ScriptableObject
    {
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
         private SpawnPosition spawnData;
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
        private string prefabName;
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
        private Color higlightColor = Color.yellow;
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
        private Color materialColor = Color.white;

        [Newtonsoft.Json.JsonIgnore]
        public SpawnPosition SpawnData { get => spawnData; set => spawnData = value; }
        [Newtonsoft.Json.JsonIgnore]
        public string PrefabName { get => prefabName; }
        [Newtonsoft.Json.JsonIgnore]
        public Color HiglightColor { get => higlightColor; }
        [Newtonsoft.Json.JsonIgnore]
        public Color MaterialColor { get => materialColor; set => materialColor = value; }

        [Newtonsoft.Json.JsonIgnore]
        public GameObject prefab;
        [Newtonsoft.Json.JsonIgnore]
        public Sprite itemSprite;
    }

    [Serializable]
    public struct SpawnPosition
    {
        public Vector3 spawnPosition;
        public Quaternion spawnRotation;

        public SpawnPosition(Vector3 spawnPosition, Quaternion spawnRotation)
        {
            this.spawnPosition = spawnPosition;
            this.spawnRotation = spawnRotation;
        }
    }
}
