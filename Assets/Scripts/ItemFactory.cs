
using Data;
using System.Linq;
using UnityEngine;

public class ItemFactory<T> where T : MovableObject
{
    internal InteractableObject Create(InteractableBaseData itemToSpawn, AddressablesItemDataBase itemDataBase, Camera mainCamera, Transform parent)
    {
        if (!itemToSpawn.prefab && !CheckPrefab(itemToSpawn, itemDataBase))
        {
            Debug.LogError("Error, no prefab for spawning the item of name " + itemToSpawn.name);
            return null;
        }

        GameObject instance = Object.Instantiate(
            itemToSpawn.prefab,
            itemToSpawn.SpawnData.spawnPosition,
            itemToSpawn.SpawnData.spawnRotation,
            parent
        );

        if (instance.TryGetComponent(out T item))
        {
            item.SetCamera(mainCamera);
            item.ChangeColor(itemToSpawn.MaterialColor);
            return item;
        }

        Debug.LogErrorFormat(
            instance,
            "Prefab instance has missing component of type: {0}.",
            typeof(T).Name
        );
        return null;
    }

    private bool CheckPrefab(InteractableBaseData itemToCheck, AddressablesItemDataBase itemDataBase)
    {
        MovableObjectData matchedData = itemDataBase.ItemList.Where(item => item.PrefabName == itemToCheck.PrefabName).FirstOrDefault();
        if (matchedData)
        {
            itemToCheck.prefab = matchedData.prefab;
            return true;
        }
        else
            return false;
    }
}
