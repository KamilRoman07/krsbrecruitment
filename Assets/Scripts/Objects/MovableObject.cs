using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : InteractableObject
{
    [SerializeField] private Rigidbody myRigidbody;
    [SerializeField] private Collider myCollider;

    private MovableObjectData movableData;
    private Camera mainCamera;
    public Camera MainCamera => mainCamera;

    private float originalRigidbodyMass;
    private RigidbodyInterpolation originalRigidbodyInterpolation;

    private bool snapToGrid = false;

    private List<Collider> isTouching = new List<Collider>();
    public List<Collider> IsTouching => isTouching;

    protected override void Awake()
    {
        base.Awake();
        if (!myRigidbody) gameObject.TryGetComponent(out myRigidbody);
        if (!myCollider) gameObject.TryGetComponent(out myCollider);
        movableData = Data<MovableObjectData>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Bound"))
            isTouching.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("Bound"))
        {
            isTouching.Remove(other);
        }
    }

    public void SetData(MovableObjectData newData)
    {
        movableData = newData;
    }

    public void SetCamera(Camera camera)
    {
        mainCamera = camera;
    }

    public override void BeginInteraction()
    {
        myRigidbody.useGravity = false;
        originalRigidbodyMass = myRigidbody.mass;
        originalRigidbodyInterpolation = myRigidbody.interpolation;
        myRigidbody.mass = movableData.RigidbodyDragWeight;
        myRigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        myRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    public override void UpdateInteraction()
    {
        // Prevent body from uncontrollable floating
        ResetVelocity(myRigidbody);
        var cameraZDistance = mainCamera.WorldToScreenPoint(transform.position).z;
        if (snapToGrid)
        {
            Vector3 moveTo = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraZDistance));
            transform.position = new Vector3(Mathf.RoundToInt(moveTo.x), Mathf.RoundToInt(moveTo.y), Mathf.RoundToInt(moveTo.z));
        }
        else
        {
            Vector3 velocity = (mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraZDistance)) - transform.position) * movableData.RigidbodyDragSpeedModifier;
            myRigidbody.velocity = velocity;
        }
    }

    private void ResetVelocity(Rigidbody rigidbody)
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }

    public override void OnInteractionEnd()
    {
        ResetVelocity(myRigidbody);
        myRigidbody.useGravity = true;
        myRigidbody.mass = originalRigidbodyMass;
        myRigidbody.interpolation = originalRigidbodyInterpolation;
        myRigidbody.constraints = RigidbodyConstraints.None;
    }

    public override void PerformSFunction(bool perform)
    {
        snapToGrid = perform;
    }
}
