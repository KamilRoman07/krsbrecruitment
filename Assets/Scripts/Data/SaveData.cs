using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "SaveData", menuName = "Data/SaveData", order = 1)]
    public class SaveData : ScriptableObject
    {
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
        private List<MovableObjectData> allMovableObjectsSaved = new List<MovableObjectData>();

        [Newtonsoft.Json.JsonIgnore]
        public List<MovableObjectData> AllMovableObjectsSaved { get => allMovableObjectsSaved; set => allMovableObjectsSaved = value; }
    }
}
