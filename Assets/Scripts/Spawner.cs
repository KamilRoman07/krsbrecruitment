using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using Cysharp.Threading.Tasks;
using System.Linq;

public class Spawner : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] private int spawnItemLimit = 30;

    private AddressablesItemDataBase itemDataBase = new AddressablesItemDataBase();
    private ItemFactory<MovableObject> factory = new ItemFactory<MovableObject>();
    private List<InteractableObject> spawnedObjects = new List<InteractableObject>();

    public AddressablesItemDataBase ItemDataBase => itemDataBase;
    public List<InteractableObject> SpawnedObjects => spawnedObjects;

    private void Awake()
    {
        itemDataBase.Initialize();
    }

    public async UniTask SpawnItem(InteractableBaseData itemToSpawn)
    {
        if (spawnItemLimit > spawnedObjects.Count)
        {
            await UniTask.WaitUntil(() => itemDataBase.AllLoaded);
            spawnedObjects.Add(factory.Create(itemToSpawn, itemDataBase, mainCamera, transform));
        }
        else
            Debug.LogError("Cannot spawn any more items, reached limit cap");
    }

    public void ClearMap()
    {
        foreach (MovableObject movable in spawnedObjects)
            Destroy(movable.gameObject);

        spawnedObjects.Clear();
    }

    public void Remove(InteractableObject objectToRemove)
    {
        List<InteractableObject> interactableInChildren = objectToRemove.GetComponentsInChildren<InteractableObject>().AsEnumerable().ToList();
        foreach (InteractableObject ioInChild in interactableInChildren)
        {
            if (ioInChild != objectToRemove && spawnedObjects.Contains(ioInChild))
            {
                spawnedObjects.Remove(ioInChild);
                Destroy(ioInChild.gameObject);
            }
        }

        if (spawnedObjects.Contains(objectToRemove))
            spawnedObjects.Remove(objectToRemove);

        Destroy(objectToRemove.gameObject);
    }
}
