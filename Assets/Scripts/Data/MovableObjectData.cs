using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data {
    [Serializable]
    [CreateAssetMenu(fileName = "MovableObjectData", menuName = "Data/MovableObjectData", order = 1)]
    public class MovableObjectData : InteractableBaseData
    {
        [Header("--MOVABLE SPECIFIC--")]
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
        private float rigidbodyDragSpeedModifier = 10f;
        [SerializeField]
        [Newtonsoft.Json.JsonProperty]
        private float rigidbodyDragWeight = 0;

        [Newtonsoft.Json.JsonIgnore]
        public float RigidbodyDragSpeedModifier { get => rigidbodyDragSpeedModifier; }
        [Newtonsoft.Json.JsonIgnore]
        public float RigidbodyDragWeight { get => rigidbodyDragWeight; }
    }
}
