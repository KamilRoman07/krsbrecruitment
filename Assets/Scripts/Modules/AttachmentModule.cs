using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AttachmentModule : MonoBehaviour
{
    [SerializeField] MainControllHUB controllHUB;
    [SerializeField] Raycast raycast;
    [SerializeField] bool checkProximity;

    public static bool attaching = false;

    private List<MovableObject> attachThisTogether;

    private void Start()
    {
        controllHUB.inputSettings.Player.Attaching.performed += AttachMode;
        controllHUB.inputSettings.Player.Attaching.canceled += AttachMode;
        controllHUB.inputSettings.Player.LeftClick.performed += SelectAttach;
        controllHUB.inputSettings.Player.RightClick.performed += RemoveAttach;
    }

    private void OnDestroy()
    {
        controllHUB.inputSettings.Player.Attaching.performed -= AttachMode;
        controllHUB.inputSettings.Player.Attaching.canceled -= AttachMode;
        controllHUB.inputSettings.Player.LeftClick.performed -= SelectAttach;
        controllHUB.inputSettings.Player.RightClick.performed -= RemoveAttach;
    }

    private void AttachMode(InputAction.CallbackContext c)
    {
        if (c.performed)
        {
            attaching = true;
            attachThisTogether = new List<MovableObject>();
        }

        if (c.canceled)
        {
            attaching = false;
            MakeAttachment();
        }
    }

    private void SelectAttach(InputAction.CallbackContext c)
    {
        if (c.performed && attaching)
        {
            if (!checkProximity)
            {
                if (raycast.CurrentCollider && raycast.CurrentCollider.TryGetComponent(out MovableObject movable) && !attachThisTogether.Contains(movable))
                    attachThisTogether.Add(movable);
            }
            else
            {
                if (raycast.CurrentCollider && raycast.CurrentCollider.TryGetComponent(out MovableObject movable) && attachThisTogether.Count == 0)
                    attachThisTogether.Add(movable);
                else if (raycast.CurrentCollider && raycast.CurrentCollider.TryGetComponent(out MovableObject movable2) && !attachThisTogether.Contains(movable2) && CheckProxy(attachThisTogether[0], movable2.gameObject))
                    attachThisTogether.Add(movable2);
            }
        }
    }

    private bool CheckProxy(MovableObject movableObject, GameObject gameObject)
    {
        if (gameObject.TryGetComponent(out Collider collider) && movableObject.IsTouching.Contains(collider))
            return true;
        else
            return false;
    }

    private void RemoveAttach(InputAction.CallbackContext c)
    {
        if (c.performed && attaching)
        {
            if (raycast.CurrentCollider && raycast.CurrentCollider.TryGetComponent(out MovableObject movable) && attachThisTogether.Contains(movable))
                attachThisTogether.Remove(movable);
        }
    }

    private void MakeAttachment()
    {
        if (attachThisTogether.Count >= 2)
        {
            GameObject attachment = CreateBaseGameObjectInTheMiddle();
            AddBases(attachment);
        }
    }

    private GameObject CreateBaseGameObjectInTheMiddle()
    {
        Vector3 position = Vector3.zero;
        foreach (MovableObject o in attachThisTogether)
        {
            position += o.transform.position;
        }
        position /= attachThisTogether.Count;
        GameObject attachement = new GameObject();
        attachement.transform.position = position;
        attachement.name = "Attachment";
        return attachement;
    }

    private void AddBases(GameObject attachment)
    {
        attachment.AddComponent<Rigidbody>();
        CreateCollider(attachment);
        MovableObject mo = attachment.AddComponent<MovableObject>();
        MovableObjectData data = ScriptableObject.CreateInstance<MovableObjectData>();
        mo.SetData(data);
        mo.SetCamera(attachThisTogether[0].MainCamera);
    }

    private void CreateCollider(GameObject attachment)
    {
        BoxCollider newCollider = attachment.AddComponent<BoxCollider>();
        Bounds bounds = new Bounds(attachment.transform.position, Vector3.one);
        foreach(MovableObject mo in attachThisTogether)
        {
            mo.transform.SetParent(attachment.transform);
            if (mo.gameObject.TryGetComponent(out Renderer renderer))
                bounds.Encapsulate(renderer.bounds);
            if(mo.TryGetComponent(out Rigidbody rigidbody)) Destroy(rigidbody);
            if (mo.TryGetComponent(out Collider collider)) Destroy(collider);
        }
        newCollider.size = bounds.extents * 2;
        newCollider.center = new Vector3(bounds.center.x, 0, bounds.center.z);
    }
}
