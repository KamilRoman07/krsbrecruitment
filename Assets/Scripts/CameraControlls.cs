using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraControlls : MonoBehaviour
{
    [SerializeField] private MainControllHUB controllHUB;
    [SerializeField] private CinemachineFreeLook cinemachineFreeLook;

    private Vector2 lookDelta;
    private bool rotateCamera;

    private void Start()
    {
        controllHUB.inputSettings.Player.MiddleClick.performed += StartRotating;
        controllHUB.inputSettings.Player.Look.performed += OnLook;
        controllHUB.inputSettings.Player.Look.canceled += OnLook;
        controllHUB.inputSettings.Player.MiddleClick.canceled += StopRotating;
    }

    private void Update()
    {
        if(rotateCamera)
            RotateCamera(lookDelta.x, lookDelta.y);
    }

    private void OnDestroy()
    {
        controllHUB.inputSettings.Player.MiddleClick.performed -= StartRotating;
        controllHUB.inputSettings.Player.Look.performed -= OnLook;
        controllHUB.inputSettings.Player.Look.canceled -= OnLook;
        controllHUB.inputSettings.Player.MiddleClick.canceled -= StopRotating;
    }

    private void StartRotating(InputAction.CallbackContext c)
    {
        rotateCamera = true;
    }

    private void StopRotating(InputAction.CallbackContext c)
    {
        rotateCamera = false;
    }    
    
    private void OnLook(InputAction.CallbackContext c)
    {
        lookDelta = c.ReadValue<Vector2>();
    }

    public void RotateCamera(float xAxisFactor, float yAxisFactor)
    {
        var rotationSpeed = 1.0f;
        if (cinemachineFreeLook)
        {
            cinemachineFreeLook.m_XAxis.Value = -xAxisFactor * rotationSpeed;
            cinemachineFreeLook.m_YAxis.Value = Mathf.Clamp(cinemachineFreeLook.m_YAxis.Value + yAxisFactor * 0.01f, 0f, 1f);
        }
    }
}
