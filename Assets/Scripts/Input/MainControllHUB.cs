using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MainControllHUB : MonoBehaviour
{
    [SerializeField] private PlayerInput playerInput;

    public InputSettings inputSettings;

    private void Awake()
    {
        inputSettings = new InputSettings();
        playerInput.actions = inputSettings.asset;
    }
}
