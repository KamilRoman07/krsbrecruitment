using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private LayerMask layerMask = -1;
    [SerializeField] private string[] ignoredTags;
    [SerializeField] private string[] priorityTags;

    private RaycastHit hit;

    private Collider currentCollider;
    public Collider CurrentCollider => currentCollider;


    private void Update()
    {
        var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        var choosenLenght = Mathf.Infinity;

        if (ProjectUtility.ClosestPriorityHit(ray, choosenLenght, out hit, layerMask, ignoredTags, false, priorityTags))
            currentCollider = hit.collider;
        else
            currentCollider = null;

    }
}
