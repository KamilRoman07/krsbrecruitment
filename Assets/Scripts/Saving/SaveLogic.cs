using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class SaveLogic
{
	public static byte[] SerializeToBytes<T>(T playerData)
	{
		return Encoding.UTF8.GetBytes(SerializeData(playerData));
	}

	public static T DeserializeFromBytes<T>(byte[] playerData, T desObject)
	{
		return DeserializeData<T>(Encoding.UTF8.GetString(playerData), desObject);
	}

	public static string SerializeData<T>(T playerData)
	{
		try
		{
			return JsonConvert.SerializeObject(playerData, Formatting.Indented, new JsonConverter[] { new vector3JsonConverter(), new quaternionJsonConverter(), new colorJsonConverter() });
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message);
		}

		return null;
	}

	public static T DeserializeData<T>(string playerData, T desObject)
	{
		try
		{
			JsonConvert.PopulateObject(playerData, desObject,
				new JsonSerializerSettings
				{
					TypeNameHandling = TypeNameHandling.Auto,
					Formatting = Formatting.Indented,
					Error = (sender, args) =>
					{
						Debug.LogError("JsonConvert.DeserializeData error: " + GetDebugObjectInfo(sender) + " Args: " + GetDebugObjectInfo(args.CurrentObject));
						if (System.Diagnostics.Debugger.IsAttached)
						{
							System.Diagnostics.Debugger.Break();
						}
						args.ErrorContext.Handled = true;
					},

				});
			return desObject;
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message);
			return default(T);
		}
	}

	private static string GetDebugObjectInfo(object obj)
	{
		if (obj == null)
			return "NULL";

		string type = "[" + obj.GetType().ToString() + "]";
		string val = "";
		try
		{
			val = obj.ToString();
		}
		catch (Exception e)
		{
			val += "error: " + e.Message;
		}

		return type + " " + val;
	}

	public class vector3JsonConverter : JsonConverter<Vector3>
	{
		public override void WriteJson(JsonWriter writer, Vector3 value, JsonSerializer serializer)
		{
			writer.WriteStartObject();

			writer.WritePropertyName(nameof(value.x));
			writer.WriteValue(value.x);

			writer.WritePropertyName(nameof(value.y));
			writer.WriteValue(value.y);

			writer.WritePropertyName(nameof(value.z));
			writer.WriteValue(value.z);

			writer.WriteEndObject();
		}

		public override Vector3 ReadJson(JsonReader reader, Type objectType, Vector3 existingValue, bool hasExistingValue, JsonSerializer serializer)
		{
			var result = Vector3.zero;

			while (reader.TokenType != JsonToken.EndObject)
			{
				reader.Read();
				if (reader.TokenType != JsonToken.PropertyName) continue;

				var property = reader.Value;
				if (property is nameof(Vector3.x)) result.x = (float)reader.ReadAsDouble();
				if (property is nameof(Vector3.y)) result.y = (float)reader.ReadAsDouble();
				if (property is nameof(Vector3.z)) result.z = (float)reader.ReadAsDouble();
			}

			return result;
		}
	}

	public class quaternionJsonConverter : JsonConverter<Quaternion>
	{
		public override void WriteJson(JsonWriter writer, Quaternion value, JsonSerializer serializer)
		{
			writer.WriteStartObject();

			writer.WritePropertyName(nameof(value.x));
			writer.WriteValue(value.x);

			writer.WritePropertyName(nameof(value.y));
			writer.WriteValue(value.y);

			writer.WritePropertyName(nameof(value.z));
			writer.WriteValue(value.z);

			writer.WritePropertyName(nameof(value.w));
			writer.WriteValue(value.w);

			writer.WriteEndObject();
		}

		public override Quaternion ReadJson(JsonReader reader, Type objectType, Quaternion existingValue, bool hasExistingValue, JsonSerializer serializer)
		{
			var result = Quaternion.identity;

			while (reader.TokenType != JsonToken.EndObject)
			{
				reader.Read();
				if (reader.TokenType != JsonToken.PropertyName) continue;

				var property = reader.Value;
				if (property is nameof(Quaternion.x)) result.x = (float)reader.ReadAsDouble();
				if (property is nameof(Quaternion.y)) result.y = (float)reader.ReadAsDouble();
				if (property is nameof(Quaternion.z)) result.z = (float)reader.ReadAsDouble();
				if (property is nameof(Quaternion.w)) result.w = (float)reader.ReadAsDouble();
			}

			return result;
		}
	}
	public class colorJsonConverter : JsonConverter<Color>
	{
		public override void WriteJson(JsonWriter writer, Color value, JsonSerializer serializer)
		{
			writer.WriteStartObject();

			writer.WritePropertyName(nameof(value.r));
			writer.WriteValue(value.r);

			writer.WritePropertyName(nameof(value.g));
			writer.WriteValue(value.g);

			writer.WritePropertyName(nameof(value.b));
			writer.WriteValue(value.b);

			writer.WritePropertyName(nameof(value.a));
			writer.WriteValue(value.a);

			writer.WriteEndObject();
		}

		public override Color ReadJson(JsonReader reader, Type objectType, Color existingValue, bool hasExistingValue, JsonSerializer serializer)
		{
			var result = Color.white;

			while (reader.TokenType != JsonToken.EndObject)
			{
				reader.Read();
				if (reader.TokenType != JsonToken.PropertyName) continue;

				var property = reader.Value;
				if (property is nameof(Color.r)) result.r = (float)reader.ReadAsDouble();
				if (property is nameof(Color.g)) result.g = (float)reader.ReadAsDouble();
				if (property is nameof(Color.b)) result.b = (float)reader.ReadAsDouble();
				if (property is nameof(Color.a)) result.a = (float)reader.ReadAsDouble();
			}

			return result;
		}
	}
}
