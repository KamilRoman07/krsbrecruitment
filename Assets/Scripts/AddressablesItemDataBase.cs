using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AddressablesItemDataBase
{
    private List<MovableObjectData> itemList = new List<MovableObjectData>();
    public List<MovableObjectData> ItemList => itemList;

    private bool itemsLoaded = false;
    public bool AllLoaded => itemsLoaded;

    public void Initialize()
    {
        Addressables.InitializeAsync().Completed += ItemDataBase_Completed;
    }

    void ItemDataBase_Completed(AsyncOperationHandle<IResourceLocator> obj)
    {
        Addressables.LoadAssetsAsync<MovableObjectData>("MovableObjectData", null).Completed += objects =>
        {
            foreach (var obj in objects.Result)
                itemList.Add(obj);
            itemsLoaded = true;
        };
    }
}
