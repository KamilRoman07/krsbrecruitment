using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorButton : MonoBehaviour
{
    [SerializeField] Color color;
    [SerializeField] InteractModule interactableModule;

    public void ChangeColor()
    {
        interactableModule.SelectedInteractableRightClick.ChangeColor(color);
    }
}
