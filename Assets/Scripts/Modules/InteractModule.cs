using UnityEngine;
using UnityEngine.InputSystem;

public class InteractModule : MonoBehaviour
{
    [SerializeField] Spawner spawner;
    [SerializeField] MainControllHUB controllHUB;
    [SerializeField] Raycast raycast;

    [SerializeField] Canvas parentCanvas;
    [SerializeField] GameObject dropDown;

    private InteractableObject selectedInteractable;
    private InteractableObject selectedInteractableRightClick;
    public InteractableObject SelectedInteractableRightClick => selectedInteractableRightClick;
    private bool interact;

    private void Start()
    {
        controllHUB.inputSettings.Player.LeftClick.performed += InteractLeftClick;
        controllHUB.inputSettings.Player.LeftClick.canceled += InteractLeftClick;
        controllHUB.inputSettings.Player.RightClick.performed += InteractRightClick;
        controllHUB.inputSettings.Player.EnableSnapping.performed += ActivateItemSFunction;
        controllHUB.inputSettings.Player.EnableSnapping.canceled += ActivateItemSFunction;
    }

    private void Update()
    {
        if (!selectedInteractable || !interact)
            return;

        selectedInteractable.UpdateInteraction();
    }

    private void OnDestroy()
    {
        controllHUB.inputSettings.Player.LeftClick.performed -= InteractLeftClick;
        controllHUB.inputSettings.Player.LeftClick.canceled -= InteractLeftClick;
        controllHUB.inputSettings.Player.RightClick.performed -= InteractRightClick;
        controllHUB.inputSettings.Player.EnableSnapping.performed -= ActivateItemSFunction;
        controllHUB.inputSettings.Player.EnableSnapping.canceled -= ActivateItemSFunction;
    }

    private void InteractLeftClick(InputAction.CallbackContext c)
    {
        if (c.performed && !AttachmentModule.attaching)
        {
            if (raycast.CurrentCollider && raycast.CurrentCollider.TryGetComponent(out InteractableObject interactable))
            {
                if (selectedInteractable == null)
                {
                    if (interactable) interactable.OnInteractionEnd();
                    selectedInteractable = interactable;
                    selectedInteractable.BeginInteraction();
                    interact = true;
                }
            }
            else
            {
                if (selectedInteractable) selectedInteractable.OnInteractionEnd();
                selectedInteractable = null;
            }
        }

        if(c.canceled && !AttachmentModule.attaching)
        {
            if (selectedInteractable)
            {
                selectedInteractable.OnInteractionEnd();
                selectedInteractable = null;
            }
            interact = false;
        }
    }

    private void InteractRightClick(InputAction.CallbackContext c)
    {
        if (c.performed && !AttachmentModule.attaching)
        {
            if (raycast.CurrentCollider && raycast.CurrentCollider.TryGetComponent(out InteractableObject interactable))
            {
                dropDown.SetActive(true);
                Vector2 movePos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    parentCanvas.transform as RectTransform,
                    Input.mousePosition, parentCanvas.worldCamera,
                    out movePos);
                dropDown.transform.position = parentCanvas.transform.TransformPoint(movePos);
                selectedInteractableRightClick = interactable;
            }
        }
    }

    private void ActivateItemSFunction(InputAction.CallbackContext c)
    {
        if (c.performed && selectedInteractable && !AttachmentModule.attaching)
            selectedInteractable.PerformSFunction(true);

        if (c.canceled && selectedInteractable && !AttachmentModule.attaching)
            selectedInteractable.PerformSFunction(false);
    }
}
