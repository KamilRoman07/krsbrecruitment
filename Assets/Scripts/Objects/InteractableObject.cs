using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    [SerializeField] private InteractableBaseData data;
    private InteractableBaseData originalData;

    protected virtual void Awake()
    {
        SetData();
    }
    protected void SetData()
    {
        if (data != null && !data.name.Contains("Clone"))
        {
            originalData = data;
            data = Instantiate(originalData);
        }
    }

    public void Save()
    {
        data.SpawnData = new SpawnPosition(transform.position, transform.rotation);
    }

    public T Data<T>() where T : InteractableBaseData
    {
        return data as T;
    }

    public virtual void BeginInteraction()
    {

    }

    public virtual void UpdateInteraction()
    {

    }

    public virtual void OnInteractionEnd()
    {

    }

    public virtual void ChangeColor(Color color)
    {
        List<MovableObject> movableInChildren = gameObject.GetComponentsInChildren<MovableObject>().AsEnumerable().ToList();
        foreach (MovableObject moInChild in movableInChildren)
            if(moInChild != this)
                moInChild.ChangeColor(color);
            
        if (gameObject.TryGetComponent(out Renderer renderer))
        {
            Material copyMaterial = Instantiate(renderer.material);
            copyMaterial.name = renderer.material.name;
            copyMaterial.SetColor("_Color", color);
            data.MaterialColor = color;
            renderer.material = copyMaterial;
        }
        else
        {
            Debug.LogError("Cannot change color of this particiular item");
        }
    }

    public virtual void PerformSFunction(bool perform)
    {

    }
}
