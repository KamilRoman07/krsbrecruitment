using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Data;
using Cysharp.Threading.Tasks;

public class SpawnItemButton : MonoBehaviour
{
    [SerializeField] Button button;

    public void SetUp(MovableObjectData objectToSpawnData, Spawner spawner)
    {
        button.image.sprite = objectToSpawnData.itemSprite;
        button.onClick.AddListener(()=>
        {
            spawner.SpawnItem(objectToSpawnData).Forget();
        });
    }
}
