using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using TMPro;

public class SpawnPanelController : MonoBehaviour
{
    [SerializeField] GameObject buttonPrefab;
    [SerializeField] Transform itemContent;
    [SerializeField] Spawner spawner;
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Transform arrow;

    private int openedX = 760;
    private int closedX = 1100;

    private string openedText = "Hide items";
    private string closedText = "Show items";

    bool opened = false;
    bool isOpening = false;

    private void Awake()
    {
        CreateSpawnItemButtons().Forget();
    }

    private async UniTask CreateSpawnItemButtons()
    {
        await UniTask.WaitUntil(() => spawner.ItemDataBase.AllLoaded);
        foreach(MovableObjectData itemData in spawner.ItemDataBase.ItemList)
        {
            GameObject button = Instantiate(buttonPrefab, itemContent);
            if(button.gameObject.TryGetComponent(out SpawnItemButton spawnItemButton))
            {
                spawnItemButton.SetUp(itemData, spawner);
            }
        }
    }

    public void ShowHidePanel()
    {
        if (!isOpening)
        {
            isOpening = true;
            if (opened)
                Close();
            else
                Open();
            opened = !opened;
        }
    }

    private void Close()
    {
        LeanTween.move(gameObject.GetComponent<RectTransform>(), new Vector3(closedX, 0, 0f), 0.5f).setOnComplete(() => isOpening = false);
        text.SetText(closedText);
        arrow.Rotate(Vector3.up, 180f);
    }

    private void Open()
    {
        LeanTween.move(gameObject.GetComponent<RectTransform>(), new Vector3(openedX, 0f, 0f), 0.5f).setOnComplete(() => isOpening = false);
        text.SetText(openedText);
        arrow.Rotate(Vector3.up, 180f);
    }

    public void OnPointerExit()
    {
        StartCoroutine(WaitAndClose(5f));
    }

    IEnumerator WaitAndClose(float timeLimit)
    {
        yield return new WaitForSecondsRealtime(timeLimit);
        if(opened)
            Close();
        opened = !opened;
    }

    public void OnPointerEnter()
    {
        StopAllCoroutines();
    }
}
