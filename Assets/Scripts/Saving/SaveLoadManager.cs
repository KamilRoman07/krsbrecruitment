using Cysharp.Threading.Tasks;
using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveLoadManager : MonoBehaviour
{
    [SerializeField] Spawner spawner;

    public static string SaveDataPath
    {
        get
        {
            return Application.persistentDataPath;
        }
    }

    public void Save()
    {
        SaveData save = ScriptableObject.CreateInstance<SaveData>();
        save.AllMovableObjectsSaved = new List<MovableObjectData>();
        foreach (InteractableObject spawnedObject in spawner.SpawnedObjects)
        {
            spawnedObject.Save();
            save.AllMovableObjectsSaved.Add(spawnedObject.Data<MovableObjectData>());
        }

        try
        {
            byte[] data = SaveLogic.SerializeToBytes(save);
            FileStream fileStream = File.Create(SaveDataPath + "/Save.dat");
            fileStream.Write(data, 0, data.Length);
            fileStream.Close();
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    public void Load()
    {
        SaveData save = ScriptableObject.CreateInstance<SaveData>();
        try
        {
            var filesDir = new DirectoryInfo(SaveDataPath);
            var files = filesDir.GetFiles($"Save*");
            foreach (var file in files)
            {
                byte[] dataBytes;
                FileStream fileStream = file.Open(FileMode.Open);
                dataBytes = new byte[fileStream.Length];
                fileStream.Read(dataBytes, 0, dataBytes.Length);
                fileStream.Close();
                if (dataBytes == null)
                {
                    Debug.LogError("Invalid data!");
                    return;
                }
                save = SaveLogic.DeserializeFromBytes(dataBytes, save);
                if (save == null)
                {
                    Debug.LogError("Invalid data!");
                    return;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            throw;
        }

        spawner.ClearMap();
        foreach (MovableObjectData objectData in save.AllMovableObjectsSaved)
        {
            spawner.SpawnItem(objectData).Forget();
        }
    }
}
