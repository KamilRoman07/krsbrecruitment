using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropdownController : MonoBehaviour
{
    [SerializeField] Spawner spawner;
    [SerializeField] InteractModule interactModule;
    [SerializeField] GameObject colorPallete;

    private void OnEnable()
    {
        colorPallete.SetActive(false);
    }

    public void DeleteItem()
    {
        spawner.Remove(interactModule.SelectedInteractableRightClick);
        gameObject.SetActive(false);
    }
}
